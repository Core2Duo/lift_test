from django.views.generic import ListView, View
from liftup_test.models import Host
from django.http import JsonResponse, HttpResponse
from django.db.utils import IntegrityError
import re

url_regexp_string = '(?:(?:https?:\/{2})?(?:((?:[0-9a-z_-]+\.)+(?:aero|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|cz|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mn|mn|mo|mp|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|nom|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ra|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw|arpa))(?::[0-9]+)?(?:(?:\/(?:[~0-9a-zA-Z\#\+\%@\.\/_-]+))?(\?[0-9a-zA-Z\+\%@\/&\[\];=_-]+)?)?))'
url_regexp = re.compile(url_regexp_string, re.IGNORECASE)


class IndexView(ListView):
    template_name = 'liftup_test/index.html'
    model = Host

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['regexp'] = url_regexp_string.replace('\\', '\\\\')
        return context


class AddView(View):
    def post(self, request):
        if 'url' not in request.POST:
            return HttpResponse(status=400)

        # Do NOT trust a client and check the url on the server side
        url = request.POST['url']
        matches = url_regexp.match(url)

        if not matches:
            # Bad URL
            return HttpResponse(status=400)

        url = matches.group(1).lower()
        try:
            host = Host(host=url)
            host.save()
        except IntegrityError:
            # Duplicate entry
            return HttpResponse(status=400)

        return JsonResponse({
            'url': host.host,
            'created': host.created.utcnow().timestamp(),
        })
