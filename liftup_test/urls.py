from django.conf.urls import url
from liftup_test.views import *

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^add/$', AddView.as_view(), name='add'),
]
