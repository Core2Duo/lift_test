from django.apps import AppConfig


class LifutpTestConfig(AppConfig):
    name = 'lifutp_test'
