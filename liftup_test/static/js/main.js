$(function(){

    var url_container = $('#url-list');

    var URL = function(data) {
        data = data || {};
        var _this = this;
        this.url = data.url;
        this.created = data.created || 0;
        this.state = data.state || URL.DELAYED;

        // Adding the element on the page
        this.el = $('<li class="clearfix" id="url-item-' + this.url + '">' + this.url + '<div class="state"></div></li>');
        this.st = this.el.find('.state');
        url_container.prepend(this.el);

        this.setState = function(state) {
            this.state = state;
            this.st.removeClass('delayed ready sending');
            switch (state) {
                case URL.DELAYED:
                    this.st.addClass('delayed');
                    this.st.html('Delayed');
                    break;
                case URL.SENDING:
                    this.st.addClass('sending');
                    this.st.html('Sending...');
                    break;
                case URL.READY:
                    this.st.addClass('ready');
                    this.st.html('Sent!');
                    setTimeout(function(){
                        _this.st.fadeOut(500);
                    }, 2000);
                    break;
            }
        };
        this.send = function() {
            if (this.state != URL.DELAYED) {
                return;
            }
            this.setState(URL.SENDING);
            $.ajax({
                type: 'POST',
                url: config.host_add_url,
                data: {
                    url: _this.url
                },
                dataType: 'json'
            }).done(function(data){
                _this.setState(URL.READY);
                _this.created = data.created;
            }).fail(function(response){
                if (response.status == 400) {
                    // Duplicate or wrong request, delete this URL
                    _this.el.remove();
                }
                if (response.readyState == 0) {
                    // Connection error, set URL state to DELAYED
                    _this.setState(URL.DELAYED);
                }
            });
        };

        URL.urls.push(this);
    };

    URL.urls = [];
    URL.load = function(arr) {
        var el;
        for (var i = 0, l = arr.length; i < l; i++) {
            el = arr[i];
            el['state'] = URL.READY;
            URL.urls.push(el);
        }
    };

    URL.SENDING = 0;
    URL.DELAYED = 1;
    URL.READY = 2;

    URL.getDelayed = function() {
        var result = [];
        for (var i = 0, l = URL.urls.length; i < l; i++) {
            if (URL.urls[i]['state'] == URL.DELAYED) {
                result.push(URL.urls[i]);
            }
        }
        return result;
    };

    URL.find = function(url) {
        for (var i = 0, l = URL.urls.length; i < l; i++) {
            if (URL.urls[i]['url'] == url) {
                return i;
            }
        }
        return -1;
    };

    var form = $('#url-form');
    var input = $('#url-input');
    var help_text = $('#help-text');

    URL.load(config.objects);

    function validate(url) {
        var re = new RegExp(config.regex, 'i');

        var matches = url.match(re);
        if (!matches) {
            return false;
        }

        url = matches[1];
        if (url.substr(0, 4) == 'www.') {
            url = url.substr(4);
        }

        return url;
    }


    input.keydown(function(){
        help_text.fadeOut(200);
    });

    form.submit(function(event) {
        event.preventDefault();
        var url = $.trim(input.val());

        url = validate(url);
        if (! url) {
            help_text.fadeIn(200);
            return;
        }

        url = url.toLowerCase();
        if (URL.find(url) != -1) {
            // Duplicate entry, clear & return
            input.val('');
            return;
        }

        url = new URL({
            url: url
        });
        input.val('');
        url.send();
    });

    // Handling delayed urls
    setInterval(function(){
        var delayed = URL.getDelayed();
        for (var i = 0, l = delayed.length; i < l; i++) {
            delayed[i].send();
        }
    }, 15000);
});