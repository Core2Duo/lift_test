from django.db import models
from django.utils import timezone


class HostManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().order_by('-created')


class Host(models.Model):
    host = models.CharField(max_length=255, unique=True)
    created = models.DateTimeField(default=timezone.now)

    objects = HostManager()
